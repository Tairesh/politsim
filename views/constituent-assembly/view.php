<?php

use app\helpers\Html;
use app\helpers\LinkCreator;

/* @var $this yii\web\View */
/* @var $model app\models\government\ConstituentAssembly */
/* @var $user app\models\auth\User */

$this->title = "Учредительное собрание в регионе «{$model->region->name}»";

$isHaveMembership = $user->isHaveAssemblyMembership($model->id);

?>

<section class="content-header">
    <h1>
	<?= Html::encode($this->title) ?>
    </h1>
</section>
<section class="content">
    <div class="row">
        <div class="col-md-12">
            <?php if ($model->dateFinished && $model->state): ?>
            <div class="callout callout-success">
                <h4><i class="icon fa fa-check"></i> Учредительное собрание завершило свою деятельность</h4>

                <p>Это учредительное собрание завершило свою деятельность <?= Html::timeAutoFormat($model->dateFinished) ?></p>
                <p>Было сформировано государство <?= LinkCreator::stateLink($model->state) ?></p>
            </div>
            <?php elseif ($model->dateFinished && !$model->state): ?>
            <div class="callout callout-danger">
                <h4><i class="icon fa fa-ban"></i> Учредительное собрание завершило свою деятельность</h4>

                <p>Это учредительное собрание завершило свою деятельность <?= Html::timeAutoFormat($model->dateFinished) ?></p>
            </div>
            <?php endif ?>
            <div class="row">
                <div class="col-md-6 col-xs-12">
                    <div class="box">
                        <div class="box-body">
                            <p>
                                <strong><i class="fa fa-calendar"></i></strong>
                                Учредительное собрание собрано <?= Html::timeAutoFormat($model->dateCreated) ?>
                            </p>
                            <p>
                                <strong><i class="fa fa-map"></i> Регион:</strong>
                                <?= LinkCreator::regionLink($model->region) ?>
                            </p>
                            <p>
                                <strong><i class="fa fa-user"></i> Инициатор собрания:</strong>
                                <?= LinkCreator::userLink($model->creator) ?>
                            </p>
                        </div>
                    </div>
                </div>
                <div class="col-md-6 col-xs-12">
                    <div class="box">
                        <div class="box-header">
                            <h4 class="box-title">Участники учредительного собрания</h4>
                        </div>
                        <div class="box-body">
                            <?php foreach ($model->users as $user): ?>
                            <p>
                                <?= LinkCreator::userLink($user) ?>
                            </p>
                            <?php endforeach ?>
                        </div>
                    </div>
                </div>
                <div class="clearfix"></div>
                <div class="col-md-12">
                    <div class="box">
                        <div class="box-header">
                            <h4 class="box-title"><i class="fa fa-paragraph"></i> Название государства</h4>
                        </div>
                        <div class="box-body">
                        <?php if (true): ?>
                            <h5>Предложенные варианты:</h5>
                            <ul>
                            <?php foreach ($model->getVariants('name') as $userId => $variant): ?>
                                <li><?= $variant['full'] ?> (<?= $variant['short'] ?>)</li>
                            <?php endforeach ?>
                            </ul>
                        <?php else: ?>
                            <p>Нет ни одного варианта</p>
                        <?php endif ?>
                            <div class="btn-group">
                                <?= Html::button('Предложить вариант', ['class' => 'btn btn-primary btn-flat', 'onclick' => 'ajaxModal("/constituent-assembly/add-variant", {assemblyId: '.$model->id.', article: "name"},  "Добавить вариант названия государства")']) ?>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="clearfix"></div>
                <div class="col-md-12">
                    <div class="box">
                        <div class="box-header">
                            <h4 class="box-title">Доступные действия</h4>
                        </div>
                        <div class="box-body">
                        <?php if ($isHaveMembership): ?>
                            <p>Вы являетесь участником этого учредительного собрания</p>
                            <p>Участникам нужно подготовить проект конституции будущего государства</p>
                            <pre>
                                <?php var_dump($model->data); ?>
                            </pre>
                        <?php else: ?>
                            <div class="button-group">
                                <?= Html::a('Принять участие в учредительном собрании', ['/constituent-assembly/request', 'id' => $model->id], ['data-method' => 'POST', 'class' => 'btn btn-primary']) ?>
                            </div>
                        <?php endif ?>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>