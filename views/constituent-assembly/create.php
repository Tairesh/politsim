<?php

use app\helpers\Html;
use app\helpers\LinkCreator;
use yii\widgets\ActiveForm;


/* @var $this yii\web\View */
/* @var $model app\models\government\ConstituentAssembly */
/* @var $region app\models\map\Region */

$this->title = Yii::t('app', 'Create Constituent Assembly');

?>

<section class="content-header">
    <h1>
        <?=Html::encode($this->title)?>
    </h1>
</section>
<section class="content constituent-assembly-create">
    <div class="row">
        <div class="col-md-12">
            <?php $form = ActiveForm::begin(); ?>
            <?= $form->errorSummary([$model]) ?>
            <p><?= Yii::t('app', 'You trying to create new constituent assembly') ?></p>
            <p><?= Yii::t('app', 'Region: ') ?> <?= LinkCreator::regionLink($region) ?></p>
            
            <?= $form->field($model, 'regionId')->hiddenInput()->label(false) ?>

            <div class="form-group">
                <?= Html::submitButton(Yii::t('app', 'Create'), ['class' => 'btn btn-success']) ?>
            </div>

            <?php ActiveForm::end(); ?>
        </div>
    </div>
</section>
