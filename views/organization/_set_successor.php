<?php

use app\helpers\Html;
use kartik\widgets\Select2;
use yii\helpers\ArrayHelper;
use app\helpers\LinkCreator;
use yii\widgets\ActiveForm;

/* @var $this \yii\web\View */
/* @var $model app\models\politics\OrganizationPost */
/* @var $user \app\models\User */
/* @var $candidats \app\models\User[] */

?>
<?php $form = ActiveForm::begin([
    'action' => ['/organization/set-successor'],
    'method' => 'POST',
]) ?>
<div class="form-group">
<?= Html::hiddenInput('postId', $model->id, ['id' => 'successor-post-id'])?>
<?= Select2::widget([
    'name'=>'userId',
    'id'=>'successor-user-id-input',
    'data' => ArrayHelper::map($candidats, 'id', 'name'),
    'pluginOptions' => [
        'escapeMarkup' => new yii\web\JsExpression("function(m) { return m; }"),
        'placeholder' => "Select successor",
    ],
]) ?>    
</div>
<div class="form-group">
    <?=Yii::t('app', 'Current successor')?>: 
    <span id="current-successor-span"><?=$model->successor ? LinkCreator::userLink($model->successor) : '<span class="text-red">'.Yii::t('app', 'Not set').'</span>'?></span>
</div>
<div class="form-group">
    <?=Yii::t('app', 'New successor')?>: 
    <span id="new-successor-span"><?=$model->successor ? LinkCreator::userLink($model->successor) : '<span class="text-red">'.Yii::t('app', 'Not set').'</span>'?></span>
</div>
<div class="form-group">
    <?= Html::submitButton('Сохранить', ['class' => 'btn btn-primary btn-flat']) ?>
</div>
<?php ActiveForm::end() ?>
<script>
    
    users = {
        <?php foreach ($candidats as $candidat): ?>
          <?=$candidat->id?>: '<?=LinkCreator::userLink($candidat)?>',
        <?php endforeach ?>
    };
    
    $("#successor-user-id-input").on("change", function(e) {
        userId = parseInt($("#successor-user-id-input").select2("val"));
        $('#new-successor-span').html(users[userId]);
    });

</script>
