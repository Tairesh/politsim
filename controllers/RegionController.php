<?php

namespace app\controllers;

use app\controllers\base\AppController;
use app\models\map\Region;
use yii\web\NotFoundHttpException;

/**
 * 
 */
final class RegionController extends AppController
{
    
    public function actionIndex($id)
    {
        $region = Region::findOne($id);
        if (is_null($region)) {
            throw new NotFoundHttpException('Region not found');
        }
        return $this->render('view', [
            'region' => $region,
            'user' => $this->user
        ]);
    }
    
}
