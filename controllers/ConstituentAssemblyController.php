<?php

namespace app\controllers;

use Yii;
use app\models\government\ConstituentAssembly;
use app\models\government\ConstituentAssemblyMembership as Membership;
use app\models\map\Region;
use app\controllers\base\AppController;
use yii\web\NotFoundHttpException;
use app\exceptions\NotAllowedHttpException;
use yii\filters\VerbFilter;

/**
 * ConstituentAssemblyController implements the CRUD actions for ConstituentAssembly model.
 */
class ConstituentAssemblyController extends AppController
{
    
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'request' => ['POST'],
                    'cancel-request' => ['POST'],
                ],
            ],
        ];
    }
    
    /**
     * Displays a single ConstituentAssembly model.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionIndex($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
            'user' => $this->user,
        ]);
    }

    /**
     * Creates a new ConstituentAssembly model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate($regionId)
    {
        $model = new ConstituentAssembly();
        $model->regionId = $regionId;
        $model->creatorId = $this->user->id;
        
        $allready_started = ConstituentAssembly::find()->where(['regionId' => $regionId, 'dateFinished' => NULL])->one();
        if ($allready_started) {
            return $this->redirect(['/constituent-assembly/', 'id' => $allready_started->id]);
        }
        
        $region = Region::findOne($regionId);
        if (is_null($region)) {
            throw new NotFoundHttpException(Yii::t('app', 'Region does not exist.'));
        }
        
        if ($region->state and !$region->state->dateDeleted) {
            throw new NotAllowedHttpException();
        }
        
        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            $membership = new Membership([
                'assemblyId' => $model->id,
                'userId' => $this->user->id,
            ]);
            $membership->save();
            return $this->redirect(['/constituent-assembly/', 'id' => $model->id]);
        }
        

        return $this->render('create', [
            'model' => $model,
            'region' => $region,
        ]);
    }

    function actionRequest(int $id)
    {
//        $model = $this->getModel($id);
        $membership = new Membership([
            'assemblyId' => $id,
            'userId' => $this->user->id,
        ]);
        $membership->save();
        return $this->redirect(['/constituent-assembly/', 'id' => $id]);
    }
    
    function actionAddVariant(int $assemblyId, string $article)
    {
        $model = $this->findModel($assemblyId);
        if (!$model->getMemberships()->andWhere(['userId' => $this->user->id])->exists()) {
            throw new NotAllowedHttpException();
        }
        
        if (Yii::$app->request->isPost) {
            if (!isset($model->dataObjects[$this->user->id])) {
                $model->dataObjects[$this->user->id] = new \app\models\government\ConstituentAssemblyData();
            }
            $model->dataObjects[$this->user->id]->load(Yii::$app->request->post());
            $model->save();
            return $this->redirect(Yii::$app->request->referrer);
        }
        
        // @TODO: check article in available names
        return $this->renderAjax('articles/_' . $article, [
            'model' => $model,
            'user' => $this->user,
        ]);
    }
    
    /**
     * Finds the ConstituentAssembly model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return ConstituentAssembly the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = ConstituentAssembly::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException(Yii::t('app', 'The requested page does not exist.'));
    }
}
