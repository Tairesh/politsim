<?php

namespace app\models\government;

use yii\base\Model;

/**
 * Description of ConstituentAssemblyData
 *
 * @author ilya
 */
class ConstituentAssemblyData extends Model
{
    
    public $name;
    
    public function rules()
    {
        return [
            [['name'], 'safe'],
        ];
    }
    
}
