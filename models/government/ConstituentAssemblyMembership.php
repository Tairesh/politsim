<?php

namespace app\models\government;

use Yii;
use app\models\auth\User;
use yii\behaviors\TimestampBehavior;

/**
 * This is the model class for table "constituentAssembliesMemberships".
 *
 * @property int $userId
 * @property int $assemblyId
 * @property int $dateCreated
 *
 * @property ConstituentAssembly $assembly
 * @property User $user
 */
class ConstituentAssemblyMembership extends \app\models\base\ActiveRecord
{
    
    use \app\models\base\traits\MembershipTrait;
    
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'constituentAssembliesMemberships';
    }

    /**
     * @inheritdoc
     */
    public function behaviors()
    {
	return [
	    [
		'class' => TimestampBehavior::className(),
		'createdAtAttribute' => 'dateCreated',
		'updatedAtAttribute' => false,
	    ],
	];
    }
    
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['userId', 'assemblyId'], 'required'],
            [['userId', 'assemblyId', 'dateCreated'], 'default', 'value' => null],
            [['userId', 'assemblyId', 'dateCreated'], 'integer'],
            [['userId', 'assemblyId'], 'unique', 'targetAttribute' => ['userId', 'assemblyId']],
            [['assemblyId'], 'exist', 'skipOnError' => true, 'targetClass' => ConstituentAssembly::className(), 'targetAttribute' => ['assemblyId' => 'id']],
            [['userId'], 'exist', 'skipOnError' => true, 'targetClass' => User::className(), 'targetAttribute' => ['userId' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'userId' => Yii::t('app', 'User ID'),
            'assemblyId' => Yii::t('app', 'Assembly ID'),
            'dateCreated' => Yii::t('app', 'Date Created'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getAssembly()
    {
        return $this->hasOne(ConstituentAssembly::className(), ['id' => 'assemblyId']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUser()
    {
        return $this->hasOne(User::className(), ['id' => 'userId']);
    }
    
    /**
     * @inheritdoc
     */
    public static function primaryKey()
    {
        return ['assemblyId', 'userId'];
    }
    
    /**
     * @param integer $userId
     * @return \yii\db\ActiveQuery
     */
    public static function findByUserId(int $userId)
    {
        return self::find()->andWhere(['userId' => $userId]);
    }
    
    /**
     * @param integer $assemblyId
     * @return \yii\db\ActiveQuery
     */
    public static function findByAssemblyId(int $assemblyId)
    {
        return self::find()->andWhere(['assemblyId' => $assemblyId]);
    }
}
