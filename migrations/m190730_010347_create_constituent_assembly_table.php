<?php

use yii\db\Migration;

/**
 * Handles the creation of table `constituent_assembly`.
 */
class m190730_010347_create_constituent_assembly_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('constituentAssemblies', [
            'id' => $this->primaryKey(),
            'regionId' => $this->integer()->unsigned()->notNull(),
            'creatorId' => $this->integer()->unsigned()->notNull(),
            'data' => 'JSONB NOT NULL',
            'stateId' => $this->integer()->null(),
            'dateCreated' => $this->integer()->unsigned()->notNull(),
            'dateFinished' => $this->integer()->unsigned()->null()
        ]);
        $this->createIndex('constituentAssembliesDateCreated', 'constituentAssemblies', ['dateCreated']);
        $this->createIndex('constituentAssembliesDateFinished', 'constituentAssemblies', ['dateFinished']);
        $this->addForeignKey('constituentAssemblies2regions', 'constituentAssemblies', ['regionId'], 'regions', ['id']);
        $this->addForeignKey('constituentAssemblies2states', 'constituentAssemblies', ['stateId'], 'states', ['id']);
        $this->addForeignKey('constituentAssemblies2users', 'constituentAssemblies', ['creatorId'], 'users', ['id']);
        
        
        $this->createTable('constituentAssembliesMemberships', [
            'userId' => $this->integer()->unsigned()->notNull(),
            'assemblyId' => $this->integer()->unsigned()->notNull(),
            'dateCreated' => $this->integer()->unsigned()->notNull(),
        ]);
        $this->createIndex('user2assembly', 'constituentAssembliesMemberships', ['userId', 'assemblyId'], true);
        $this->createIndex('dateCreatedAssembliesMemberships', 'constituentAssembliesMemberships', ['dateCreated']);
        $this->addForeignKey('user2assemblyForeignAssembly', 'constituentAssembliesMemberships', ['assemblyId'], 'constituentAssemblies', ['id']);
        $this->addForeignKey('user2assemblyForeignUser', 'constituentAssembliesMemberships', ['userId'], 'users', ['id']);
        
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropForeignKey('user2assemblyForeignUser', 'constituentAssembliesMemberships');
        $this->dropForeignKey('user2assemblyForeignAssembly', 'constituentAssembliesMemberships');
        $this->dropIndex('dateCreatedAssembliesMemberships', 'constituentAssembliesMemberships');
        $this->dropIndex('user2assembly', 'constituentAssembliesMemberships');
        $this->dropTable('constituentAssembliesMemberships');
        
        $this->dropIndex('constituentAssembliesDateCreated', 'constituentAssemblies');
        $this->dropIndex('constituentAssembliesDateFinished', 'constituentAssemblies');
        $this->dropForeignKey('constituentAssemblies2regions', 'constituentAssemblies');
        $this->dropForeignKey('constituentAssemblies2states', 'constituentAssemblies');
        $this->dropForeignKey('constituentAssemblies2users', 'constituentAssemblies');
        $this->dropTable('constituentAssemblies');
    }
}
