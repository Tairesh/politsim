<?php

use yii\db\Migration;
use app\models\economy\resources\Resource;

/**
 * Class m190730_190358_add_resources_to_tiles
 */
class m190730_190358_add_resources_to_tiles extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $resources = Resource::findAll();
        $data = [];
        foreach ($resources as $res) {
            $data[(int)$res->id] = 0;
        }
        $json = json_encode($data);
        $this->addColumn('tiles', 'resources', "JSONB DEFAULT '{$json}'");
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropColumn('tiles', 'resources');
    }

}
