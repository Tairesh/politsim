<?php

use yii\db\Migration;

/**
 * Handles the creation of table `constitutions`.
 */
class m190730_205452_create_constitutions_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('constitutions', [
            'id' => $this->primaryKey(),
            'stateId' => $this->integer()->unsigned()->null(),
            'data' => 'JSONB NOT NULL',
            'dateCreated' => $this->integer()->unsigned()->notNull(),
            'dateConfirmed' => $this->integer()->unsigned()->null(),
        ]);
        $this->addForeignKey('constitutionsOwnerForeign', 'constitutions', ['stateId'], 'states', ['id']);
        $this->createIndex('constitutionsDateCreated', 'constitutions', ['dateCreated']);
        $this->createIndex('constitutionsDateConfirmed', 'constitutions', ['dateConfirmed']);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropForeignKey('constitutionsOwnerForeign', 'constitutions');
        $this->dropIndex('constitutionsDateCreated', 'constitutions');
        $this->dropIndex('constitutionsDateConfirmed', 'constitutions');
        $this->dropTable('constitutions');
    }
}
